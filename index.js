//alert("hello again");

// sytanx, statements and comments
// statements- programming our machine to perform

	// JS statements - usually ends with semi-colon (;)
		
		//semi-colon helps to locate where statements end

//Syntax- set of rules describing how statements must be constructed
//Comments- are part of the codes that is ignored
	//meant to describe the written code
	
	//two-types of comments
	
		//single line comments <//>
		//multi-line </*>

/**/
// console.log = prints in the console
console.log("hello world");

//Variables- storing information

	// Declaring variables
/*
	Synatax: let/ const variableName
*/
let myVariable
console.log(myVariable) //result:undefined

//console.log(hello); // result: not defined if a variable
// let hello; cannot access before initialization

/*Guides in declari*/

//Declaring and initializaing variables
/*
Syntax: let/ const variableName=
*/
let productName = "desktop computer";
console.log (productName);

let price = 18999
console.log(price);

// let variables- you change the value of the variable

// const variables- information are values that cant be change

const interest = 3.539;
console.log(interest)

//Reassigning variables
/*
	Syntax: varibleName = value
*/

productName = "laptop"
console.log(productName);

let friend ="Min";
friend = "Jane";

console.log(friend);

//let friend= "Kate"
console.log(friend);

//interest=3.614
//console.log(interest);

let role = "Supervisor";
let name = "Edward";

/*
	Mini-activity:

	printout the value of role and name
	re assign the value of the rle to director
*/

console.log(role,name)
role = "Director"
console.log(role,name)

//Initialization- done after a variable has been declared.

let supplier;
supplier = "Jane Smith Tradings";
console.log (supplier)
// Reassign- done aftr giving initial value to teh variable
supplier = "Zuitt Store"
console.log(supplier);

// var vs. let/ const



/*const pi;
pi = 1.1416
console.log(pi);*/
//var-was used from 1997 to 2015
//let/ const - ES6 updates
//Hoisting of var

a = 5;
console.log(a);
var a;


//let / const ;oca; and global scope

//Scope- essentiallly means where these variables are available for use

//Global variable
//let outerVariable = "Hello";

// local variable
	//let /const are blocked scoped
	// block is a chunk of code surrounds by {}. A block lives in curly braces
	//let innerVariable = "Hello Again"};

	//console.log(outerVariable)// hello
	//console.log(innerVariable)// error


	// Multiple Variable Declarations

/*let productCode="DC017", productBrand = "Dell";*/

	let productCode = "DC017"
	const productBrand = "Dell";

	/*const let= "hello"
	console.log(let)*/

	/*console.log (productCode, productBrand)*/;


	//Data Types

	// Strings 
	
		// series of alphanumeric characters that create a word, phrase a sentence or anything related to text

		//strings are wrapped around quotation marks

let country = "Philipines"
let city = 'Caloocan City';
let numberString = "12345"

//Concatenating Strings

let fullAddress = city + ',' + ' ' + country;
console.log(fullAddress);

/*
mini-activity:
1. initialize a variable called myName with own name as value
2. concatenate it with the phrase 'Hi am '
. store conscatenate stg in a var calle greeting

*/
let myName = "Benedick"
let phrase = "Hi I am"
console.log(phrase, myName)
// escape character- (\)
let mailAddress = 'Metro Manila\n\nPhilippines'
console.log (mailAddress) // result line break between MM and PH

let message = "John's father is coming today"
console.log(message)
message ='John\s father is coming today'



let pokemon ="pikachu"
console.log(pokemon)

//Number

// Integers/ whole number
let count = 64;
console.log(count);

//Decimal / Frations
let grade = 98.7;
console.log(grade);

//Exponential Notations
let planetDistance = 2e10	
console.log(planetDistance)

//concatenation text and numbers

console.log('Jino\s grade last quarter is' +' ' + grade);

let numberString1 = "0912345678"

// Boolean 
// the valus relate to state of a certain thing
//T or F- useful considering logic

/*let isSingle = true;
let inGoodCOnduct = false

console.log("isSingle" +' ' + isSingle)
console.log("inGoodCOnduct" + ' ' = inGoodCOnduct)

*/
//additional info
	// 1 equivalent to true
	// 0 equivalent to false

// Arrays 
// Special kind of data type. can store 

/* let / const arrayName = [ elementA, elementB ]
*/


/*let anime = ["Naruto", "Bleach", " Attack"]
console.log(anime)

let quarterlyGrades = [95, 96.3, 87, 90]
console.log(quarterlyGrades)

// doesnt make sense in context of programming
let random = ["JK", 24, true];
console.log(random);
*/
// Objects
// Special kind of data type, mimic real world

/* let / const objectName = {
	propertyA: valueA,
	propertyB: valueB
}
key value- pairs
*/

let person = {
	fullName: "Midoriya Izuku",
	age: 15,
	isStudent: true,
	contactNo:["09123456789", "8123 4567"],
	address: { 
		houseNumber: "568",
		city: "Tokyo"
	}
}

console.log(person)


//Null
let spouse = null;
console.log(spouse);

let emptyString = ""
console.log(emptyString);

let myNumber = 0
console.log(myNumber)